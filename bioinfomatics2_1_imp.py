# -*- coding: utf-8 -*-
"""
Created on Thu Jun 16 14:53:33 2016

@author: kanemoto masayuki
"""

# -*- coding: utf-8 -*-
"""
Created on Mon Jun 13 14:28:07 2016

@author: Mitsutaka
"""
import copy
import csv
import time

DNA=["ATCCAGCT",
     "GGGCAACT",
     "ATGGATCT",
     "AAGCAACC",
     "TTGCAACT",
     "ATGCCACT",
     "ATGGCACT"]

def BranchAndBoundMotifSearch(DNA,t,n,l):
    bestScore=0
    s=[1]*t
    i=1
    rows=[]
    rows.append("l="+str(l))    
    
    while i>0:
        
        print("------------------")
        rows.append("----------------")
        print("入力i : "+str(i))
        rows.append("入力i : "+str(i))
        if n==l:
            if Score(s,t,DNA,l)>bestScore:
                bestScore=Score(s,t,DNA,l)
                bestMotif=copy.copy(s)
                print("bestMotif")
                rows.append("bestMotif")
                print(bestMotif)
                rows.append(bestMotif)
                for j in range(t):
                    print(DNA[j][bestMotif[j]-1:bestMotif[j]+l-1])
                    rows.append(DNA[j][bestMotif[j]-1:bestMotif[j]+l-1])
                print("bestScore : "+str(bestScore))
                rows.append("bestScore : "+str(bestScore))
                i=0
        elif i<t:
            optimisticScore=Score(s,i,DNA,l)+(t-i)*l
            if optimisticScore < bestScore:
                (s,i)=Bypass(s,i,t,n-l+1)                
                print(s)
                print("Bypass")  
                rows.append(s)
                rows.append("Bypass")

                
                for j in range(t):
                    print(DNA[j][s[j]-1:s[j]+l-1])
                    rows.append(DNA[j][s[j]-1:s[j]+l-1])
                print("出力i : "+str(i))
                rows.append("出力i : "+str(i))
            else:
                (s,i)=NextVertex(s,i,t,n-l+1)
                
                print(s)
                print("NextVertex") 
                rows.append([s])
                rows.append(["NextVertex"])
                for j in range(t):
                    print(DNA[j][s[j]-1:s[j]+l-1])
                    rows.append(DNA[j][s[j]-1:s[j]+l-1])
                print("出力i : "+str(i))
                rows.append("出力i : "+str(i))

        else:
            if Score(s,t,DNA,l)>bestScore:
                bestScore=Score(s,t,DNA,l)
                bestMotif=copy.copy(s)
                
                print("bestMotif")
                print(bestMotif)
                rows.append("bestMotif")
                rows.append(bestMotif)
                for j in range(t):
                    print(DNA[j][bestMotif[j]-1:bestMotif[j]+l-1])
                    rows.append(DNA[j][bestMotif[j]-1:bestMotif[j]+l-1])
                print("bestScore : "+str(bestScore))
                rows.append("bestScore : "+str(bestScore))
                
                
            (s,i)=NextVertex(s,i,t,n-l+1)
            print(s)
            print("NextVertex")   
            rows.append(s)
            rows.append("NextVertex")            
            for j in range(t):
                print(DNA[j][s[j]-1:s[j]+l-1])
                rows.append(DNA[j][s[j]-1:s[j]+l-1])
            print("出力i : "+str(i))
            rows.append("出力i : "+str(i))
    #return bestMotif
    print("------------------")
    rows.append("----------------")
    print("Last bestMotif")
    rows.append("Last bestMotif")
    print(bestMotif)
    rows.append(bestMotif)
    for j in range(t):
        print(DNA[j][bestMotif[j]-1:bestMotif[j]+l-1])
        rows.append(DNA[j][bestMotif[j]-1:bestMotif[j]+l-1])
    print("bestScore : "+str(bestScore))
    rows.append("bestScore : "+str(bestScore))
    
    return (bestMotif,rows)
 

def Bypass(a,i,L,k):
    for j in reversed(range(i)):
        if a[j]<k:
            a[j]=a[j]+1
            return (a,j+1)
    return (a,0)

def NextVertex(a,i,L,k):
    if i < L:
        a[i]=1
        return (a,i+1)
    else:
        for j in reversed(range(L)):
            if a[j]<k:
                a[j]=a[j]+1
                return (a,j+1)
    return (a,0)

def Alignment(s,DNA,l,i):   
    k=0
    alingment=[]
    for j in s:
        alingment.append(DNA[k][j-1:j+l-1])
        k+=1
        if k==i:
            break
    return alingment    
    


def Score(s,i,DNA,l):
    alignment=Alignment(s,DNA,l,i) 
    Score=0
    for k in range(l):
        profile=[0,0,0,0] 
        for j in range(len(alignment)):
            word=alignment[j][k]
            if alignment[j][k]=="A":
                profile[0]+=1
            elif alignment[j][k]=="T":
                profile[1]+=1
            elif alignment[j][k]=="G":
                profile[2]+=1
            elif alignment[j][k]=="C":
                profile[3]+=1
            else:
                pass
            
        Score=Score+max(profile)    
       
    return Score
    
            
t=len(DNA)
n=len(DNA[0])
sumrows=[]
times=[]
L=[i for i in range(1,(n+1))]

for l in L:
    start=time.time()
    (s,rows)=BranchAndBoundMotifSearch(DNA,t,n,l)
    end=time.time()-start
    times.append(end)
    sumrows.append(rows)
    
sumrows.append(times)

import matplotlib.pyplot as plt

plt.plot(L, times, 'or',label="l")
plt.legend()
plt.show()

f=open('data.csv','w',newline='')
csvWriter=csv.writer(f)
for rows in sumrows:
    csvWriter.writerow(rows)

f.close()    
