# -*- coding: utf-8 -*-
"""
Created on Mon Jun 20 14:43:24 2016

@author: Ohwada
"""


import copy
import csv
import time

def GreedyMotifSearch(DNA,t,n,l):
    bestMotif=[1]*t
    s=[1]*t
    for s1 in range(1,n-l+1):
        s[0]=s1
        for s2 in range(1,n-l+1):
            s[1]=s2
            if Score(s,2,DNA,l)>Score(bestMotif,2,DNA,l):
                bestMotif[0]=s1
                bestMotif[1]=s2
                  
                
    s[0]=bestMotif[0]
    s[1]=bestMotif[1]
    
    for i in range(3,t):
        for si in range(1,n-l+1):
            s[i-1]=si
            if Score(s,i,DNA,l) >Score(bestMotif, i ,DNA,l):
                bestMotif[i-1]=si
                
                ##hyouzi
                print("----------------------")
                print(bestMotif)
                for j in range(t):
                    print(DNA[j][bestMotif[j]-1:bestMotif[j]+l-1])
                    
                    
            s[i-1]=bestMotif[i-1]
    print("~~~LastbestMotif~~~")
    print(bestMotif)
    for j in range(t):
        print(DNA[j][bestMotif[j]-1:bestMotif[j]+l-1])
    return bestMotif
    
    
def Alignment(s,DNA,l,i):   
    k=0
    alingment=[]
    for j in s:
        alingment.append(DNA[k][j-1:j+l-1])
        k+=1
        if k==i:
            break
    return alingment    
    


def Score(s,i,DNA,l):
    alignment=Alignment(s,DNA,l,i) 
    Score=0
    for k in range(l):
        profile=[0,0,0,0] 
        for j in range(len(alignment)):
            word=alignment[j][k]
            if alignment[j][k]=="A":
                profile[0]+=1
            elif alignment[j][k]=="T":
                profile[1]+=1
            elif alignment[j][k]=="G":
                profile[2]+=1
            elif alignment[j][k]=="C":
                profile[3]+=1
            else:
                pass
            
        Score=Score+max(profile)    
       
    return Score
    
    


DNA=["ATCCAGCTATCGATCGATCGAATCTCAGCTTAGCATGTGTTTGTTAGCATGTGACACAGTGTATCGTCTC",
     "GGGCAACTATCTACGATTTGCTATTAGCATGTGCAGCCTAGCTCCTGATCGGATACCCAATGTAGCTAGG",
     "ATGGATCTCGCTCTGATCGTCGAATGTGTCGATAATTCTTAGCATGTGTCTAAGAGTGTGTCGCTAGCTA",
     "AAGCAACCATCGCTATCTGTGTAATTTAGCATGTGCGATGCTGCATGTGTATATACACCCAGATTGTGAA",
     "TTGCAACTAATTTAGCATGTGCGTAATATACGTGACCTAGTCATAAGTGTCCAGTCGATCGGATCGTACA",
     "ATGCCACTATCTATGTCCACGAGATTAGCATGTGATCGATTACGATTTCGTCCCGTGTAGTCCTATCTGT",
     "ATGGCACTCGATTAGCATGTGTTCGATCTAGGCAATCGGCTTAGAAGTCAATCGATTCGGCGATTCGGAT"]
     
     

t=len(DNA)
n=len(DNA[0])
sumrows=[]
times=[]
L=[i for i in range(1,(n+1))]


for l in L:
    start=time.time()
    print("----------------------")
    print("モチーフの長さは"+str(l))
    print("----------------------")
    bestMotif=GreedyMotifSearch(DNA,t,n,l)
    end=time.time()-start
    times.append(end)
    sumrows.append(str(l))#ここ変えた
    
sumrows.append(times)


import matplotlib.pyplot as plt

plt.plot(L, times, 'or',label="l")
plt.legend()
plt.show()

f=open('data.csv','w',newline='')
csvWriter=csv.writer(f)
for rows in sumrows:
    csvWriter.writerow(rows)

f.close()    

     
     
